package com.example.tan089.currencyconverter;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.AdapterView.*;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// The program can connect to the Internet, an undeclared permission will not be permitted.
// To get the permission to connect to the Internet, you have to declare this line in the file "AndroidManifest.xml"
//   <uses-permission android:name="android.permission.INTERNET" />

public class MainActivity extends AppCompatActivity {

    private EditText editText01;
    private Button bnt01;
    private TextView title = null;
    private TextView textView01;
    private String usd;
    private Spinner dropdown;

    // URL used to extract real-time currency exchange rates, using JSON, for the Petrol-AircraftCarrier-Federal-Reverse note.
    String json_url = "https://api.fixer.io/latest?base=USD";

    // JSON Object extracted from website.
    String JSON_String = "";

    String currentCurrency = "JPY";
    double currentUSDWorth = 100.00; // USD -> JPY, etc.

    // If the current currency changes.
    public void adjustProgramText()
    {
        if(title == null) { return; }

        title.setText(String.format("Currency Converter: USD to %s", currentCurrency));
    }

    public void createDropDown()
    {
        String[] available_currencies = new String[]{
                "AUD", "BGN", "BRL",
                "CAD", "CHF", "CNY",
                "CZK", "DKK", "EUR",
                "GBP", "HKD", "HKR",
                "HUF", "IDR", "ILS",
                "INR", "ISK", "JPY",
                "KRW", "MXN", "MYR",
                "NOK", "NZD", "PHP",
                "PLN", "RON", "RUB",
                "SEK", "SGD", "THB",
                "TRY", "ZAR",
        };

        dropdown = (Spinner) findViewById(R.id.DropDown1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, available_currencies);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dropdown.setAdapter(adapter);

        // Never use setOnItemClickListener(). Spinner never supports OnItemClickListener at all,
        // it will throw an exception and make the program crash. OnItemSelectedListener is used.
        dropdown.setOnItemSelectedListener(new OnItemSelectedListener()
        {
            public void onNothingSelected(AdapterView<?> adapterView) {}

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View v, int position, long id)
            {
                currentCurrency = (String)adapterView.getItemAtPosition(position);
                MainActivity.this.adjustProgramText();
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = findViewById(R.id.title);
        editText01 = findViewById(R.id.EditText01);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        // Set up a button listener to handle necessary events.
        bnt01.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View convertToYen)
            {
                // The result will not appear immediately.
                // The program has to retrieve the latest currency rates on the Internet, it will take some time.
                // The goal is that we will not want the main thread to be unresponsive, we will have another thread do it.
                BackgroundTask object = new BackgroundTask();

                object.execute();
            }
        });

        createDropDown();
    }

    private class BackgroundTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            // This method specifies what happens during the progress of the Asynchroronous Task executing.
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            // This is the method for the immediate aftermath of the Asynchronous Task being executed.
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(String result)
        {
            // Get USD input from the edit box
            usd = editText01.getText().toString();
            if(usd.equals(""))
            {
                textView01.setText("This field cannot be empty!");
            }
            else
            {
                // Convert a string to double
                Double usdInput = Double.parseDouble(usd);

                Double resultUSD = usdInput * currentUSDWorth;

                // Update the result on the screen
                textView01.setText("$" + String.format("%.2f", usdInput) + " = (" + currentCurrency + String.format(") %.2f", resultUSD));
            }

            // Clear the edit box
            editText01.setText("");
        }


        @Override
        protected String doInBackground(String... params)
        {
            try{
                // Create an object for the URL class and initialize it to the 'url' string in this Java class, MainActivity.
                URL web_url = new URL(MainActivity.this.json_url); // (1)

                // Create an object from the HttpURLConnection class named httpURLConnection and initialize it with
                // (HttpURLConnection) web_url.openConnection(); (URL::openConnection())
                HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();

                // Request method set to "GET".
                httpURLConnection.setRequestMethod("GET");

                httpURLConnection.connect();

                // Create an object from the class InputStream and initialize object with
                InputStream inputStream = httpURLConnection.getInputStream(); // (2)

                // Create on object named BufferedReader class and initialize the object with
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                // Extract the string from the retrieved JSON
                String line = bufferedReader.readLine();
                MainActivity.this.JSON_String = "";

                while (line != null && line.length() != 0)
                {
                    MainActivity.this.JSON_String += line;
                    line = bufferedReader.readLine();
                }

                // Create a JSON object.
                JSONObject obj = new JSONObject(MainActivity.this.JSON_String); // (3)
                JSONObject objRate = obj.getJSONObject("rates");

                MainActivity.this.currentUSDWorth = Double.parseDouble(objRate.get(MainActivity.this.currentCurrency).toString());
            }
            catch(java.net.MalformedURLException e)
            {
                e.printStackTrace();
                textView01.setText("Exception: java.net.MalformedURLException (1)");
            }
            catch(java.io.IOException e)
            {
                e.printStackTrace();
                textView01.setText("Exception: java.io.IOException (2)");
            }
            catch(org.json.JSONException e)
            {
                e.printStackTrace();
                textView01.setText("Exception: org.json.JSONException (3)");
            }
            return null;
        }
    }
}
